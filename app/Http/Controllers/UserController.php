<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use App\Common;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(Request $request) {
        if ($request->isJson()) {
            $data = $request->json()->all();
            $credentials = ["username" => $data["username"], "password" => $data["password"]];

            if (! $token = Auth::attempt($credentials)) {
                return response()->json(["success" => false, 'message' => 'Unauthorized'], 401);
            }

            $user = User::where("username", $data["username"])->first();
            $nextPage = (($user->first_name==NULL && $user->last_name==NULL) || $user->gender==NULL || $user->dob==NULL) ? "/updateProfile" : "/home";

            return response()->json([
                "success" => true,
                "token" => $token,
                "nextPage" => $nextPage
            ]);

        } else {
            return response()->json(null, 400);
        }
    }

    public function signup(Request $request) {
        if ($request->isJson()) {
            $data = $request->json()->all();
            $user = User::where('username', $data["username"])->first();
            if ($user) {
                return response()->json([
                    "success" => false,
                    "message" => "User Already Exist"
                ], 400);
            }
            else if ($data["password"]!=$data["confirmPassword"]) {
                return response()->json([
                    "success" => false,
                    "message" => "Password do not match"
                ], 400);
            }
            else {
                $user = new User;
                $user->username = $data["username"];
                $user->password = app('hash')->make($data["password"]);;
                $user->save();

                $credentials = ["username" => $data["username"], "password" => $data["password"]];

                if (! $token = Auth::attempt($credentials)) {
                    return response()->json(["success" => false, 'message' => 'Unauthorized'], 401);
                }

                return response()->json([
                    "success" => true,
                    "token" => $token,
                    "nextPage" => "/updateProfile"
                ]);
            }
        } else {
            return response()->json(null, 400);
        }
    }

    public function getProfile() {
        $userId = (JWTAuth::getPayload(JWTAuth::getToken())->toArray())["sub"];
        $user = User::where('id', $userId)->first();
        return response()->json([
            "success" => true,
            "firstName" => $user->first_name,
            "lastName" => $user->last_name,
            "dob" => $user->dob,
            "gender" => $user->gender
        ]);
    }

    public function updateProfile(Request $request) {
        if ($request->isJson()) {
            $userId = (JWTAuth::getPayload(JWTAuth::getToken())->toArray())["sub"];
            $data = $request->json()->all();
            $affectedRows = User::where('id', $userId)->update([
                "first_name" => $data["firstName"]?:NULL,
                "last_name" => $data["lastName"]?:NULL,
                "dob" => $data["dob"]?: NULL,
                "gender" => $data["gender"]?:NULL
            ]);
            if ($affectedRows>0) {
                return response()->json([
                    "success" => true
                ]);
            }
            else {
                return response()->json([
                    "success" => false
                ], 400);
            }
        } else {
            return response()->json(null, 400);
        }        
    }

    public function home() {
        $posts = [
            ["title" => "BMW 1", "media" => "https://autowise.com/wp-content/uploads/2016/09/m3-e1558381100567.jpg"],
            ["title" => "BMW 2", "media" => "https://c4.wallpaperflare.com/wallpaper/736/621/268/machine-night-street-tuning-wallpaper-preview.jpg"],
            ["title" => "BMW 3", "media" => "https://www.gangup.com/media/groupdeals/deal/cache/13/image/9df78eab33525d08d6e5fb8d27136e95/e/d/eddy-e30-m3-1.jpg"],
            ["title" => "BMW 4", "media" => "https://pictures.topspeed.com/IMG/jpg/201708/bmw-i3-68.jpg"],
            ["title" => "BMW 5", "media" => "https://i.pinimg.com/originals/7c/99/22/7c99223f192707849c9f401b068881d1.jpg"],
            ["title" => "BMW 6", "media" => "https://farm5.staticflickr.com/4874/46023337734_e1aae18b24_b.jpg"],
            ["title" => "BMW 7", "media" => "https://www.e90post.com/forums/attachment.php?attachmentid=1852546&stc=1&d=1530373285"],
            ["title" => "BMW 8", "media" => "https://live.staticflickr.com/2816/12164077603_b65a90de48_b.jpg"]
        ];
        $userId = (JWTAuth::getPayload(JWTAuth::getToken())->toArray())["sub"];
        $user = User::where('id', $userId)->first();
        
        return response()->json([
            "success" => true,
            "firstName" => $user->first_name,
            "lastName" => $user->last_name,
            "dob" => $user->dob,
            "gender" => $user->gender,
            "posts" => $posts
        ]);
    }
}
