<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/login', 'UserController@login');
$router->post('/signup', 'UserController@signup');
$router->get('/getProfile', ['middleware' => 'auth', 'uses' => 'UserController@getProfile']);
$router->post('/updateProfile', ['middleware' => 'auth', 'uses' => 'UserController@updateProfile']);
$router->get('/home', ['middleware' => 'auth', 'uses' => 'UserController@home']);
